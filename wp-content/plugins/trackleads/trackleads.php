<?php
/*
Plugin Name: MM Track Leads
Plugin URI: http://www.wp-code.com/
Description: Sends leads to the client CRM, according to the Client Code and Site Code that are defined in general settings.Client Code and Site Code must match what is defined in CDE for this client
Author: MM
Version: 1.8
Author URI: https://wpmaster.mm-dev.agency
*/


require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    //'https://bitbucket.org/user-name/repo-name',
    'https://bitbucket.org/mobilemarketingllc/trackleads',
	__FILE__,
	'trackleads'
);

$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
	'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));

//Optional: Set the branch that contains the stable release.
    $myUpdateChecker->setBranch('master');
    

add_action('wp_footer', 'mmsession_custom_footer_js', 300);
function mmsession_custom_footer_js() {
    
  echo "<script src='https://session.mm-api.agency/js/mmsession.js'></script>";
}

add_action('gform_after_submission', 'createLeadsAfterFormSubmission', 10, 2);

function createLeadsAfterFormSubmission($entry, $form) {
    write_log('tracking lead for ' . $form['title']);
    $lead = array();
    $lead['source'] = 'Website';
    $lead['form'] = $form['title'];
    $lead['entry'] = $entry;


    if (isset($_COOKIE['__ss_tk'])) {
        $lead['trackingId'] = $_COOKIE['__ss_tk'];
    }

    if (isset($_COOKIE['traffic_src'])) {
        $lead['trafficSource'] = $_COOKIE['traffic_src'];
    }

    if (isset($_COOKIE['mmsid'])) {
        $lead['mmsid'] = $_COOKIE['mmsid'];
    }

    $fields = array();

    foreach ($form['fields'] as $f) {
        $fields[] = array(
            'id' => (string)$f->id,
            'type' => $f->type,
            'label' => $f->label
        );
    }

    $lead['fields'] = $fields;
    post($lead);
}

//for contact form 7

add_action( 'wpcf7_mail_sent', 'your_wpcf7_mail_sent_function' ); 

function your_wpcf7_mail_sent_function( $contact_form ) {

    $lead = array();
    $lead['form'] = $contact_form->title;
    $lead['source'] = 'Website'; 

    if (isset($_COOKIE['__ss_tk'])) {
        $lead['trackingId'] = $_COOKIE['__ss_tk'];
    }

    if (isset($_COOKIE['traffic_src'])) {
        $lead['trafficSource'] = $_COOKIE['traffic_src'];
    }

    if (isset($_COOKIE['mmsid'])) {
        $lead['mmsid'] = $_COOKIE['mmsid'];
    }

 $submission = WPCF7_Submission::get_instance();

 if ( $submission ) {
    $lead = $submission->get_posted_data();
 }
 post($posted_data);
}


//for woocommerce order
add_action( 'woocommerce_payment_complete', 'create_invoice_for_wc_order',  1, 1  );
function create_invoice_for_wc_order( $order_id ) {
    $order = new WC_Order( $order_id );
    $order = wc_get_order( $order_id );  

    $lead = array();
    $lead['form'] = 'Woocommerce Order';
    $lead['source'] = 'Website'; 

    if (isset($_COOKIE['__ss_tk'])) {
        $lead['trackingId'] = $_COOKIE['__ss_tk'];
    }

    if (isset($_COOKIE['traffic_src'])) {
        $lead['trafficSource'] = $_COOKIE['traffic_src'];
    }

    if (isset($_COOKIE['mmsid'])) {
        $lead['mmsid'] = $_COOKIE['mmsid'];
    }

    $lead = [
        "firstname" => $order->billing_first_name,
        "lastname" => $order->billing_last_name,
        "email" => $order->get_billing_email(),
        "address1" => $order->billing_address_1,
        "address2" => $order->billing_address_2,
        "city" => $order->billing_city,
        "zipcode" => $order->billing_postcode,
        "formType" => "sampleRequest",
		"promoCode" => "smplrqsttrspns",
        "phone" => $order->billing_phone,
        "state_name" => $order->billing_state,
        "country" => $order->billing_country,
        "order_number" => $order->get_order_number(),
        "order_total" =>  $order->get_total(),
        "cart_tax" => $order->get_cart_tax(),
        "currency" =>  $order->get_currency(),
        "taxes" => $order->get_total_tax(),
        "shipping_tax" =>$order->get_shipping_tax(),
        "shipping_total" => $order->get_shipping_total(),
    ];  

    $item_name = array();

    foreach ( $order->get_items() as $item_id => $item ) {

       $price =  get_post_meta($item->get_product_id(),'_price');
       
        $item_name[] = array(
                             'product_name' => $item->get_name(),
                             'product_id' => $item->get_product_id(),                             
                             'price' => $price['0'],
                             'quantity' => $item->get_quantity()
                            );
                     
    }

    $lead['items'] = $item_name;

    write_log($lead);
    post($lead);

}

//Ninja form submission
add_action( 'ninja_forms_after_submission', 'my_ninja_forms_after_submission' );
function my_ninja_forms_after_submission( $form_data ){
//  write_log($form_data['settings']);

  $lead = array();
  $lead['source'] = 'Website';
  $lead['form'] = $form_data['settings']['title'];  


  if (isset($_COOKIE['__ss_tk'])) {
      $lead['trackingId'] = $_COOKIE['__ss_tk'];
  }

  if (isset($_COOKIE['traffic_src'])) {
      $lead['trafficSource'] = $_COOKIE['traffic_src'];
  }

  if (isset($_COOKIE['mmsid'])) {
      $lead['mmsid'] = $_COOKIE['mmsid'];
  }

  $fields = array();

  $form_fields   =  $form_data[ 'fields' ];

  foreach ($form_fields as $field) {

    if (is_array($field['value'])){

        $fvalue = $field['value'][0];

    }else{

        $fvalue = $field['value'];
    }

    $fields[] = array(
        'id' => $field['id'],
        'type' => $field['type'],
        'label' => $field['label'],
        'value' => $fvalue
    );

  }

  $lead['fields'] = $fields;
    
  write_log($lead);
  
 post($lead);
  
}

/**
 * @param array $lead
 */
function post(array $lead) {

    try {
        $ch = curl_init();

        $clientCode = get_option('SITE_CODE');
        $siteCode = "www";

        if (empty($clientCode) && defined('CLIENT_CODE')) $clientCode = CLIENT_CODE;
        if (empty($siteCode) && defined('SITE_CODE')) $siteCode = SITE_CODE;
        
        
        //IF CLIENT ID IS NOT SET IT WILL TAKE FROM COOKIES mmcc VALUE
       if (empty($clientCode)) { if (isset($_COOKIE['mmcc'])) { $clientCode= $_COOKIE['mmcc']; }  }
       if (empty($siteCode)) { $siteCode='www'; }


        if (empty($clientCode) || empty($siteCode)) {
            mail('steve@mobile-marketing.agency',
                'missing config for ' . get_option('blogname'),
                'client code=' . $clientCode . ', site code=' . $siteCode);
        }

        write_log('got client data: ' . $clientCode . '_' . $siteCode);

        write_log('got client data: ' . $clientCode . '_' . $siteCode);
        write_log("https://crm.mm-api.agency/$clientCode/$siteCode/lead");

        $url = "https://crm.mm-api.agency/$clientCode/$siteCode/lead";
        $json = json_encode($lead);
        write_log("sending $json to $url");

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($json)));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if (curl_exec($ch) === false) {
            write_log(curl_error($ch));
        }

        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ('200' != $status) {
            mail('steve@mobile-marketing.agency', 'failed lead submission', $status . ' for ' . $url . '\n\n' . $json);
            write_log($status . ' for ' . $url . '\n' . $json);
        };

        curl_close($ch);
    } catch (Exception $e) {
        write_log('error executing curl: ' . $e->getMessage());
        mail('steve@mobile-marketing.agency', 'curl issue on site', $e->getMessage());
    }
}

if (!function_exists('write_log')) {
    function write_log($log) {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}