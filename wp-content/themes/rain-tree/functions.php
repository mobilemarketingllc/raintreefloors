<?php

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab

    return $tabs;

}


// Hiding Prices on all Free Sample Products
add_filter( 'woocommerce_get_price_html', function( $price, $product ) {
	if ( is_admin() ) return $price;
	// Hide for these category slugs / IDs
	$hide_for_categories = array( 'free-samples' );
	// Don't show price when its in one of the categories
	if ( has_term( $hide_for_categories, 'product_cat', $product->get_id() ) ) {
		return '';
	}
	return $price; // Return original price
}, 10, 2 );
add_filter( 'woocommerce_cart_item_price', '__return_false' );
add_filter( 'woocommerce_cart_item_subtotal', '__return_false' );



/**
 * Remove Categories from WooCommerce Product Category Widget
 *
 * @author   Ren Ventura
 */

//* Used when the widget is displayed as a dropdown
add_filter( 'woocommerce_product_categories_widget_dropdown_args', 'rv_exclude_wc_widget_categories' );
 //* Used when the widget is displayed as a list
add_filter( 'woocommerce_product_categories_widget_args', 'rv_exclude_wc_widget_categories' );
function rv_exclude_wc_widget_categories( $cat_args ) {
	$cat_args['exclude'] = array('127','99','100','101','166'); // Insert the product category IDs you wish to exclude
	return $cat_args;
}

/*add_action( 'woocommerce_before_order_notes', 'bbloomer_add_custom_checkout_field' );
  
function bbloomer_add_custom_checkout_field( $checkout ) { 
   
   $saved_license_no = "sampleRequest";
   woocommerce_form_field( 'formType', array(        
      'type' => 'hidden',        
       
      'default' => $saved_license_no,        
   ), $checkout->get_value( 'license_no' ) ); 
}*/
add_action( 'woocommerce_before_order_notes', 'additional_hidden_checkout_field' );
function additional_hidden_checkout_field() {
    echo '<input type="hidden" name="promoCode" value="smplrqsttrspns">';
    echo '<input type="hidden" name="formType" value="sampleRequest">';
}
/**
 * @snippet       Save & Display Custom Field @ WooCommerce Order
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.8
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
add_action( 'woocommerce_checkout_update_order_meta', 'bbloomer_save_new_checkout_field' );
  
function bbloomer_save_new_checkout_field( $order_id ) { 
    if ( $_POST['formType'] ) update_post_meta( $order_id, '_formType', esc_attr( $_POST['formType'] ) );
	if ( $_POST['promoCode'] ) update_post_meta( $order_id, '_promoCode', esc_attr( $_POST['promoCode'] ) );
}
/**
 * @snippet       Clear the checkout fields. 
 */
add_filter('woocommerce_checkout_get_value','__return_empty_string',10);